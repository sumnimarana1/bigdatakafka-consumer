# kafka-producer-twitter

> A Java app that reads from a Kafka topic.

## Prerequisities

* [7zip Windows 64-bit](https://www.7-zip.org/download.html)
* JDK8
* [Apache Maven Binary zipfile](https://maven.apache.org/download.cgi)
* Zookeeper
* [Kafka Binary for Scala 2.11](https://kafka.apache.org/downloads)
* Maven

Configure system environment variables for easy access. Create the following - use your path.

```Bash
JAVA_HOME
KAFKA_HOME
MAVEN_HOME
ZOOKEEPER_HOME
```

System Path should include

```Bash
%JAVA_HOME%\bin
%KAFKA_HOME%\bin
%KAFKA_HOME%\bin\windows
%MAVEN_HOME%\bin
%ZOOKEEPER_HOME%\bin
```

My paths are:

```Bash
C:\apache-maven-3.6.0
C:\jdk1.8.0_181
C:\kafka_2.11-2.0.0
C:\tools\zookeeper-3.4.9
```

Verify installation by opening PowerShell in the project folder and checking version.

```PowerShell
java -version
mvn -v
```

## Start ZooKeeper on port 2181

In Windows File Explorer, right-click on your project folder and "Open PowerShell Here as Administrator". Run the batch file to start the Zookeeper service. Keep this window open to keep the service running.

```PowerShell
zookeeper-server-start.bat zookeeper.properties
```

## Start Kafka

Open another PowerShell (or Command Window) as Admin and start Kafka. Keep this window open to keep Kafka running.

```PowerShell
kafka-server-start.bat server.properties
```

Kafka will start and connect to ZooKeeper.

## View topics 

Open another Command Window as Admin and list all topics.

```dos
kafka-topics.bat --list --zookeeper localhost:2181
```

## Create Java Executable

1. Explore the Maven pom file and the Java code.
2. Open a PowerShell window in the project folder, compile the code using Maven and create an executable jar file. Generated artificacts can be found in the new 'target' folder.

```PowerShell
mvn clean compile assembly:single
```

## Start the App

To start the app, use java -classpath and provide the jar and executable class:

```PowerShell
java -cp target/CaseConsumer-1.0-SNAPSHOT-jar-with-dependencies.jar edu.nwmissouri.isl.professorcase.kafka.CaseConsumer
```

## Working with Kafka

To list topics, open PowerShell as admin and run:

```PowerShell
kafka-topics.bat --list --zookeeper localhost:2181
```

To delete a topic (e.g. test), open PowerShell as admin and run:

```PowerShell
kafka-topics.bat --zookeeper localhost:2181 --delete --topic test

To describe a topic (e.g. test), open PowerShell as admin and run:

```PowerShell
kafka-topics.bat --describe --zookeeper localhost:2181 --topic test
```

To view the messages on a topic (e.g. test), open PowerShell as admin and run:

```PowerShell
kafka-console-consumer.bat --zookeeper localhost:2181 --topic test --from-beginning
```

To list the groups, open PowerShell as admin and run:

```PowerShell
kafka-consumer-groups.bat --zookeeper localhost:9092 --list
```

## See also

- <http://cloudurable.com/blog/kafka-tutorial-kafka-producer/index.html>
- <https://bitbucket.org/professorcase/h07>
- <https://bitbucket.org/professorcase/h08>
- <https://bitbucket.org/professorcase/kafka-producer-twitter>

